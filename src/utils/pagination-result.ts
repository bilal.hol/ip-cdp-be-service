import { IPaginationOptions } from './types/pagination-options';
import { PaginationResultType } from './types/pagination-result.type';

export const paginationResult = <T>(
  data: T[],
  options: IPaginationOptions,
): PaginationResultType<T> => {
  return {
    data,
    hasNextPage: data.length === options.limit,
  };
};
