import { PartialType } from '@nestjs/mapped-types';
import { CreateCompanyDto } from './create-company.dto';
import { IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateCompanyDto extends PartialType(CreateCompanyDto) {
  @ApiProperty({ example: 'PT. XYZ Enterprise' })
  @IsOptional()
  companyName: string;

  @ApiProperty({ example: 'XYZ Street' })
  @IsOptional()
  companyAddress: string;

  @ApiProperty({ example: 'Law Firm' })
  @IsOptional()
  businessType: string;

  @ApiProperty({ example: 'www.XYZ.com' })
  @IsOptional()
  companyWebsite: string;

  @ApiProperty({ example: 'contact@xyz.com' })
  @IsOptional()
  companyEmail: string;

  @ApiProperty({ example: '13413423' })
  @IsOptional()
  companyPhone: string;

  @ApiProperty({ example: 'Indonesia  ' })
  @IsOptional()
  companyCountry: string;
}
