import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateCompanyDto {
  @ApiProperty({ example: 'PT. XYZ Enterprise' })
  @IsNotEmpty()
  companyName: string;

  @ApiProperty({ example: 'XYZ Street' })
  @IsNotEmpty()
  companyAddress: string;

  @ApiProperty({ example: 'Law Firm' })
  @IsNotEmpty()
  businessType: string;

  @ApiProperty({ example: 'www.XYZ.com' })
  @IsNotEmpty()
  companyWebsite: string;

  @ApiProperty({ example: 'contact@xyz.com' })
  @IsNotEmpty()
  companyEmail: string;

  @ApiProperty({ example: '13413423' })
  @IsNotEmpty()
  companyPhone: string;

  @ApiProperty({ example: 'Indonesia  ' })
  @IsNotEmpty()
  companyCountry: string;
}
