import { Injectable } from '@nestjs/common';
import { CreateCompanyDto } from './dto/create-company.dto';
import { UpdateCompanyDto } from './dto/update-company.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Company } from './entities/company.entity';
import { DeepPartial, Repository } from 'typeorm';
import { IPaginationOptions } from 'src/utils/types/pagination-options';
import { EntityCondition } from 'src/utils/types/entity-condition.type';
import { NullableType } from 'src/utils/types/nullable-type';

@Injectable()
export class CompanyService {
  constructor(
    @InjectRepository(Company)
    private companyRepository: Repository<Company>,
  ) {}

  create(createCompanyDto: CreateCompanyDto) {
    return this.companyRepository.save(
      this.companyRepository.create(createCompanyDto),
    );
  }

  findAll(paginationOptions: IPaginationOptions): Promise<Company[]> {
    return this.companyRepository.find({
      skip: (paginationOptions.page - 1) * paginationOptions.limit,
      take: paginationOptions.limit,
    });
  }

  findOne(fields: EntityCondition<Company>): Promise<NullableType<Company>> {
    return this.companyRepository.findOne({
      where: fields,
    });
  }

  update(
    id: Company['id'],
    updateCompanyDto: DeepPartial<UpdateCompanyDto>,
  ): Promise<Company> {
    return this.companyRepository.save({
      id,
      ...updateCompanyDto,
    });
  }

  async remove(id: Company['id']): Promise<void> {
    await this.companyRepository.delete(id);
  }
}
