import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateCompanyTable1693208210397 implements MigrationInterface {
  name = 'CreateCompanyTable1693208210397';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE \`company\` (\`id\` int NOT NULL AUTO_INCREMENT, \`companyName\` varchar(255) NOT NULL, \`companyAddress\` varchar(255) NOT NULL, \`businessType\` varchar(255) NOT NULL, \`companyWebsite\` varchar(255) NOT NULL, \`companyEmail\` varchar(255) NOT NULL, \`companyPhone\` varchar(255) NOT NULL, \`companyCountry\` varchar(255) NOT NULL, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deletedAt\` datetime(6) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE \`company\``);
  }
}
