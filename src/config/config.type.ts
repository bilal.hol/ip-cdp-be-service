export type AppConfig = {
  nodeEnv: string;
  name: string;
  port: number;
  apiPrefix: string;
};

export type DatabaseConfig = {
  type?: string;
  host?: string;
  port?: number;
  password?: string;
  username?: string;
  name?: string;
  synchronize?: boolean;
};

export type AllConfigType = {
  app: AppConfig;
  database: DatabaseConfig;
};
